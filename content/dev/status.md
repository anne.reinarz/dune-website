+++
title = "Build Status"
[menu.main]
parent = "dev"
weight = 6
+++

## Core Modules

<table>
  <thead>
    <tr>
      <th>module</th>
      <th>master</th>
      <th>2.6</th>
      <th>2.5</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>dune-common</td>
      <td><a href="https://gitlab.dune-project.org/core/dune-common/commits/master"><img src="https://gitlab.dune-project.org/core/dune-common/badges/master/build.svg"></a></td>
      <td><a href="https://gitlab.dune-project.org/core/dune-common/commits/releases/2.6"><img src="https://gitlab.dune-project.org/core/dune-common/badges/releases/2.6/build.svg"></a></td>
      <td><a href="https://gitlab.dune-project.org/core/dune-common/commits/releases/2.5"><img src="https://gitlab.dune-project.org/core/dune-common/badges/releases/2.5/build.svg"></a></td>
    </tr>
    <tr>
      <td>dune-geometry</td>
      <td><a href="https://gitlab.dune-project.org/core/dune-geometry/commits/master"><img src="https://gitlab.dune-project.org/core/dune-geometry/badges/master/build.svg"></a></td>
      <td><a href="https://gitlab.dune-project.org/core/dune-geometry/commits/releases/2.6"><img src="https://gitlab.dune-project.org/core/dune-geometry/badges/releases/2.6/build.svg"></a></td>
      <td><a href="https://gitlab.dune-project.org/core/dune-geometry/commits/releases/2.5"><img src="https://gitlab.dune-project.org/core/dune-geometry/badges/releases/2.5/build.svg"></a></td>
    </tr>
    <tr>
      <td>dune-grid</td>
      <td><a href="https://gitlab.dune-project.org/core/dune-grid/commits/master"><img src="https://gitlab.dune-project.org/core/dune-grid/badges/master/build.svg"></a></td>
      <td><a href="https://gitlab.dune-project.org/core/dune-grid/commits/releases/2.6"><img src="https://gitlab.dune-project.org/core/dune-grid/badges/releases/2.6/build.svg"></a></td>
      <td><a href="https://gitlab.dune-project.org/core/dune-grid/commits/releases/2.5"><img src="https://gitlab.dune-project.org/core/dune-grid/badges/releases/2.5/build.svg"></a></td>
    </tr>
    <tr>
      <td>dune-grid-howto</td>
      <td><a href="https://gitlab.dune-project.org/core/dune-grid-howto/commits/master"><img src="https://gitlab.dune-project.org/core/dune-grid-howto/badges/master/build.svg"></a></td>
      <td><a href="https://gitlab.dune-project.org/core/dune-grid-howto/commits/releases/2.6"><img src="https://gitlab.dune-project.org/core/dune-grid-howto/badges/releases/2.6/build.svg"></a></td>
      <td><a href="https://gitlab.dune-project.org/core/dune-grid-howto/commits/releases/2.5"><img src="https://gitlab.dune-project.org/core/dune-grid-howto/badges/releases/2.5/build.svg"></a></td>
    </tr>
    <tr>
      <td>dune-istl</td>
      <td><a href="https://gitlab.dune-project.org/core/dune-istl/commits/master"><img src="https://gitlab.dune-project.org/core/dune-istl/badges/master/build.svg"></a></td>
      <td><a href="https://gitlab.dune-project.org/core/dune-istl/commits/releases/2.6"><img src="https://gitlab.dune-project.org/core/dune-istl/badges/releases/2.6/build.svg"></a></td>
      <td><a href="https://gitlab.dune-project.org/core/dune-istl/commits/releases/2.5"><img src="https://gitlab.dune-project.org/core/dune-istl/badges/releases/2.5/build.svg"></a></td>
    </tr>
    <tr>
      <td>dune-localfunctions</td>
      <td><a href="https://gitlab.dune-project.org/core/dune-localfunctions/commits/master"><img src="https://gitlab.dune-project.org/core/dune-localfunctions/badges/master/build.svg"></a></td>
      <td><a href="https://gitlab.dune-project.org/core/dune-localfunctions/commits/releases/2.6"><img src="https://gitlab.dune-project.org/core/dune-localfunctions/badges/releases/2.6/build.svg"></a></td>
      <td><a href="https://gitlab.dune-project.org/core/dune-localfunctions/commits/releases/2.5"><img src="https://gitlab.dune-project.org/core/dune-localfunctions/badges/releases/2.5/build.svg"></a></td>
    </tr>
  </tbody>
</table>

## Staging Modules

<table>
  <thead>
    <tr>
      <th>module</th>
      <th>master</th>
      <th>2.6</th>
      <th>2.5</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>dune-functions</td>
      <td><a href="https://gitlab.dune-project.org/staging/dune-functions/commits/master"><img src="https://gitlab.dune-project.org/staging/dune-functions/badges/master/build.svg"></a></td>
      <td>-</td>
      <td><a href="https://gitlab.dune-project.org/staging/dune-functions/commits/releases/2.5"><img src="https://gitlab.dune-project.org/staging/dune-functions/badges/releases/2.5/build.svg"></a></td>
    </tr>
    <tr>
      <td>dune-python</td>
      <td><a href="https://gitlab.dune-project.org/staging/dune-python/commits/master"><img src="https://gitlab.dune-project.org/staging/dune-python/badges/master/build.svg"></a></td>
      <td>-</td>
      <td>-</td>
    </tr>
    <tr>
      <td>dune-typetree</td>
      <td><a href="https://gitlab.dune-project.org/staging/dune-typetree/commits/master"><img src="https://gitlab.dune-project.org/staging/dune-typetree/badges/master/build.svg"></a></td>
      <td>-</td>
      <td><a href="https://gitlab.dune-project.org/staging/dune-typetree/commits/releases/2.5"><img src="https://gitlab.dune-project.org/staging/dune-typetree/badges/releases/2.5/build.svg"></a></td>
    </tr>
    <tr>
      <td>dune-uggrid</td>
      <td><a href="https://gitlab.dune-project.org/staging/dune-uggrid/commits/master"><img src="https://gitlab.dune-project.org/staging/dune-uggrid/badges/master/build.svg"></a></td>
      <td><a href="https://gitlab.dune-project.org/staging/dune-uggrid/commits/releases/2.6"><img src="https://gitlab.dune-project.org/staging/dune-uggrid/badges/releases/2.6/build.svg"></a></td>
      <td><a href="https://gitlab.dune-project.org/staging/dune-uggrid/commits/releases/2.5"><img src="https://gitlab.dune-project.org/staging/dune-uggrid/badges/releases/2.5/build.svg"></a></td>
    </tr>
  </tbody>
</table>

## Extension Modules

<table>
  <thead>
    <tr>
      <th>module</th>
      <th>master</th>
      <th>2.6</th>
      <th>2.5</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>dune-grid-glue</td>
      <td><a href="https://gitlab.dune-project.org/extensions/dune-grid-glue/commits/master"><img src="https://gitlab.dune-project.org/extensions/dune-grid-glue/badges/master/build.svg"></a></td>
      <td>-</td>
      <td><a href="https://gitlab.dune-project.org/extensions/dune-grid-glue/commits/releases/2.5"><img src="https://gitlab.dune-project.org/extensions/dune-grid-glue/badges/releases/2.5/build.svg"></a></td>
    </tr>
  </tbody>
</table>
