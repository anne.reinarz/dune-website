+++
date = "2015-09-25T15:44:13+01:00"
major_version = 2
minor_version = 4
modules = ["dune-common", "dune-istl", "dune-geometry", "dune-grid", "dune-localfunctions", "dune-grid-howto"]
patch_version = 0
title = "Dune 2.4.0"
version = "2.4.0"
signed = 1
[menu]
  [menu.main]
    parent = "releases"
    weight = -1
+++

# DUNE 2.4 - Release Notes

*   The minimal required compiler are GCC 4.4 (GCC 4.7 on OSX, see [this bug](https://gitlab.dune-project.org/flyspray/FS/issues/1590)) and Clang 3.4\. We try to stay compatible to ICC 15, and newer. Older versions of Clang might work, ICC 14.0.3 needs patches to systems headers.

## buildsystem

*   The default buildsystem has been switched from Autotools to CMake. For Dune 2.4 you can use Autotools by adding `--no-cmake` to dunecontrol. (see our CMake documentation, [this bug](https://gitlab.dune-project.org/flyspray/FS/issues/1500)).
*   If MPI is found, the build is parallel. It is no longer necessary to explicitly enable its support. It can still be disabled by passing `-DCMAKE_DISABLE_FIND_PACKAGE_MPI=TRUE` for CMake and `--disable-parallel` for Autotools.
*   Some convenience macros for automatically copying or symlinking files from the source tree to the build tree are provided in `dune-common/cmake/modules/DuneSymlinkOrCopy.cmake`
*   With CMake headercheck is disabled by default as it creates a host of files in the build directory. You can re-enable it by adding `-DENABLE_HEADERCHECK=1` to the CMake flags.

### Deprecated and removed features

*   The use of Autotools is deprecated and will be removed after Dune 2.4\. Use CMake instead.
*   The macros `HAVE_DUNE_HASH`, `HAVE_INTEGRAL_CONSTANT`, `HAVE_RVALUE_REFERENCES`, `HAVE_STD_HASH`, `HAVE_TYPE_TRAITS`, `HAVE_VARIADIC_CONSTRUCTOR_SFINAE` and, `HAVE_VARIADIC_TEMPLATES`, defined in `config.hh`, are deprecated and will be removed after Dune 2.4\. This applies to various `HAVE_TR1_*`, too. The variables are always true for the minimal required compiler versions.

## dune-common

*   dune-common now provides a (simple) implementation of `bash-completion` for the `dunecontrol` command.
*   `FieldMatrix` and `FieldVector` now offer construction from initializer lists.
*   `CollectiveCommunication` is now default-constructible.
*   Objects of type `DiagonalMatrix` can now be constructed from `initializer_list`s.

### Deprecated and removed features

*   The class `SelectType` (from `typetraits.hh`), deprecated in Dune 2.3, has been removed. Use `conditional` instead.
*   The deprecated file `misc.hh` has been removed. Most math-related stuff has moved to `math.hh`. The two methods `hasPrefix` and `hasSuffix` are now in the new header `stringutility.hh`. The methods `SQR` and `genFilename` disappear with no replacement.
*   Support for very old MPI implementations, specifically implementations without support for the MPI-2.1 standard, has been removed. It was deprecated in Dune 2.3\. This should affect very few users because all common MPI packages have been compatible with that standard since 2009.
*   Support for compilers that lack `std::thread`. Thread support is needed to implement a thread-safe quadrature rule cache, see [this bug](https://gitlab.dune-project.org/flyspray/FS/issues/1544). All compilers officially supported by dune have `std::thread`. Note however that support on OS X for GCC < 4.7 was dropped because of the lacking support for `std::thread`.

## dune-geometry

*   The quadrature rule cache `QuadratureRules` is now thread safe, see [this bug](https://gitlab.dune-project.org/flyspray/FS/issues/1544).
*   There are new types `Codim<cd>` and `Dim<d>` that can be used to encapsulate a (co)dimension. They inherit from `std::integral_constant<int>` and are useful when an interface should accept both a dimension and a codimension.

### Deprecated and removed features

*   The 1d quadrature rules `Gauss`, `Jacobian_1_0`, and `Jacobian_2_0` have been removed. Please use the rules `GaussLegendre`, `GaussJacobi_1_0`, and `GaussJacobi_2_0`.
*   The class `QuadraturePoint` does not export the variable `d` and the type `CoordType` anymore. Please use `dimension` and `Field` instead.
*   The methods `global`, `mapping`, `volumeOuterNormal`, and `initializeTopology` have been removed from the `ReferenceElement` class.
*   The methods `ReferenceElement::global` have been removed; use `ReferenceElement::geometry<codim>(i).global` instead. The non-interface method `ReferenceElement::mapping`, which is superseded by the `geometry` method, has been deprecated, too.
*   The classes `GenericReferenceElement*` were renamed as `ReferenceElement*` in Dune 2.3\. The old, deprecated names have been removed.
*   The class `MockGeometry`, deprecated in Dune 2.3, has been removed. In most cases, replacing it by `MultiLinearGeometry` and adapting the includes will do.
*   The header `genericgeometry/geometry.hh` and its containing classes `Geometry`, `GenericGeometry`, and `LocalGeometry`, which were all deprecated in Dune 2.3, have been removed.

## dune-grid

*   The minimum required version of `UG` is now `UG-3.11.0`. Note that you need a bugfix provided by `UG-3.11.1` if you want to build `UG` on your own with Clang.
*   `UGGrid` now allows to do load balancing with the element partitioning being provided by third-party software. There is no restriction on what software to use here. All you need to do is to set up a `std::vector` which for each element contains the rank where this element is supposed to be sent to. Everything else happens automatically.
*   The results of `Geometry::jacobianTransposed` and `Geometry::jacobianInverseTransposed` are now returned by value rather than by reference. The result types are guaranteed to be copyable and assignable. In the unlikely event that you store references or pointers to these returned values, this change might break your code without a word of warning.
*   The class `Entity<0>` has a new method `subEntities(uint codim)`, which returns the number of subentities of the given codimension. It has the same functionality as the method `count`, but the `codim` argument of the new method is normal parameter, rather than a template parameter. Also, we believe that the new name is more telling. The `count` method is deprecated.
*   `IdentityGrid` has been moved from dune-grid-dev-howto to dune-grid.
*   Support for the external Grid `Alberta` older then the current release 3 has been abandoned without deprecation.
*   The mapper classes `SingleCodimSingleGeometryMapper` and `MultipleCodimMultipleGeometryMapper` now use the number type used by the grid index set to return indices. Previously, `int` was hard-wired.
*   It is now possible to use C++11 [range-based for loops](http://en.cppreference.com/w/cpp/language/range-for) to iterate over entities and intersections. See the new section [Iterating over grid entities and intersections](http://www.dune-project.org/doc/doxygen/html/group__GIIteration.html#_details) in the Doxygen documentation for further details. This feature is not available on all supported compilers, it requires GCC 4.6 or greater (or another compiler that supports range-based for loops).
*   All iterators over entities are now [forward iterators](http://www.cplusplus.com/reference/iterator/ForwardIterator/) in the sense of the standard library. In particular, they can now be default-constructed and postfix-incremented, which wasn't possible previously. The usual caveat concerning postfix increments applies: postfix incrementing may be noticeably slower than prefix incrementing.
*   The various `Mapper` classes in `dune/grid/common` now have methods `index` and `subIndex`, which do the same thing as the `map` methods. With this renaming the `Mapper` interface is more consistent with the conceptually similar `IndexSet` interface. The old `map` methods are still there, but they are marked as deprecated and will be removed after the 2.4 release.
*   There is a new method `types` on index sets that returns an iterator range visiting all geometry types of a given codimension contained in the domain of the index set. Its type is implementation defined and exported as typedef `Types`.
*   A new helper class `ParMetisGridPartitioner` allows to use ParMetis to compute partitionings for arbitrary Dune grids. Currently, `UGGrid` can handle such partitionings.
*   Entities can now be compared with each other.
*   `TensorGridFactory`, a factory class for tensor product grids can be found in `dune/grid/utility/tensorgridfactory.hh`. It is implemented through the `GridFactory` for all unstructured grids and has a specialization for `YaspGrid`. The factory class provides a multitude methods to fill coordinate ranges. Check the doxygen documentation for details.

### Copyable entities and intersections

`Entity`s and `Intersection`s can now be copied and default-constructed. This will allow us to remove the `EntityPointer`, which has been deprecated in DUNE 2.4\. User-visible changes include:

*   Most obviously: Instead of storing an `EntityPointer` or an `IntersectionIterator`, it is now possible to directly store a copy of an `Entity` or an `Intersection`.
*   Iterators are now allowed to return temporary Entity or Intersection objects instead of references. Code that captures the result in a const reference will still work in both cases, because the life time of the temporary is bound to this reference. Important interface change: Code that forwards such references may fail due to dangling references. In order to avoid this you can forward with the correct return type using `decltype(*it)`. I.e. you have to change `const Entity& foo(const Iterator& it) { return *it;}` to `auto foo(const Iterator& it) -> decltype(*it) { return *it;}` .
*   Important: Keep in mind that `Entity` and `Intersection` objects are expensive. Don't try to store lots of them! If you need to keep around a list of entities, use `EntitySeed` instead.
*   All interface methods that used to return an `EntityPointer` now return an `Entity` instead. This change applies to the following methods:
    *   `Entity::father()` for entities of codimension 0.
    *   `Entity::subEntity<codim>()` for entities of codimension 0.
    *   `Intersection::inside()`
    *   `Intersection::outside()`
    *   `Grid::entityPointer(const EntitySeed&)`. This method has been deprecated, please use the new method `Grid::entity(const EntitySeed&)` instead, which returns an `Entity`
*   `Entity` and `EntityPointer` have a certain amount of interoperability code to ease the transition. In particular, you can dereference an entity with `*entity` and call member methods with `entity->foo()` so that your code works with both grids that have been ported to the new interfaces as well as unported grids. All of the compatibility methods do however raise deprecation warnings.
*   All grid implementations in `dune-grid` have been ported to the new interface, except for the deprecated bindings for ALUGrid 1.52\. If you use ALUGrid, you should switch to the new `dune-alugrid` module.
*   While ported and unported grids are mostly interoperable, there is one important exception: You cannot use meta grids that use the new interface (like `GeometryGrid` with host grids that still use the old interface.

#### YaspGrid

The YaspGrid grid manager has received an important overhaul. Changes include:

*   YaspGrid now has a second template parameter, which specifies how coordinates are stored. This parameter defaults to `EquidistantCoordinates<ctype, dim>` which mimics the old behavior.
*   As a new feature, YaspGrid now has the capabilities of a tensor-product grid. This can be enabled via the second template parameter `TensorProductCoordinates<ctype, dim>`. Constructor calls for a tensor-product grid use `Dune::array<std::vector<ctype>, dim>` as a data structure for the coordinates of the grid.
*   Changing the coordinate type of YaspGrid can now be done by switching the template parameter of the coordinate container.
*   `YaspGrid` is (again) working for arbitrary dimension.
*   `YaspGrid` implements entities of arbitrary codimension and can communication on these entities. With this addition, `YaspGrid` fulfills all requirements of a DUNE grid (as decided upon in Aachen 2013).
*   You can obtain an equidistant `YaspGrid` with non-trivial origin by using `EquidistantOffsetCoordinates<ctype, dim>` as the second template parameter. This feature can also be used through `StructuredGridFactory`
*   There are only three constructors for a `Yaspgrid`: One for equidistant grids, one for equidistant grids with non-trivial origin and one for tensorproduct grids. All other constructors are either deprecated or can still be used due to proper default values on the new constructors.
*   `YaspGrid` now makes consequent use of `CollectiveCommunication`. As these classes have just been made default-constructible, the communicator parameter can usually be omitted on the constructor call.
*   A `BackupRestoreFacility` for YaspGrid is implemented in the header `<dune/grid/yaspgrid/backuprestore.hh>`. Equidistant grids write a single file for all processes, tensorproduct grids write one file per processor, that only contains the coordinate range relevant to that processor. The output format is a self-defined ascii format which is readable by the human eye.
*   All partitioning code is now located in `dune/grid/yaspgrid/partitioning.hh`. Note that if you previously implemented your own partitioner, you need to update its signature to use `std::array<int, dim>` instead of `Dune::FieldVector<int, dim>`. `YaspFixedSizePartitioner` is a new implementation of a partitioner with a predefined number of processors per direction.
*   `YaspGrid` now provides a native implementation of `PersistentContainer`.

### Deprecated and removed features

*   The `SGrid` grid manager has been marked as deprecated, and will be removed after the 2.4 release. `YaspGrid` now supports all features of `SGrid`, so there is no reason left to have two structured grid managers in dune-grid. To suppress the SGrid deprecation warning define the macro `DUNE_AVOID_SGRID_DEPRE_WARNING_BECAUSE_I_KNOW_WHAT_IM_DOING`.
*   The class `Geometry` does not export the type `Jacobian` anymore. Please use the type `JacobianInverseTransposed` instead.
*   The class `Entity` does not export the type `ctype` anymore. Please use the type `Entity::Geometry::ctype` instead.
*   The class `Entity` does not export the number `dimensionworld` anymore. Please use `Entity::Geometry::dimensionworld` instead.
*   The methods `EntityIterator::level` and `EntityPointer::level` have been deprecated. To obtain the level of an entity pointed to by an iterator or EntityPointer, please dereference the iterator/pointer and call the method `level` on the entity directly.
*   The class `EntityPointer` does not export `codim` anymore. Please use the type `codimension` instead (see [this bug](https://gitlab.dune-project.org/flyspray/FS/issues/1477)).
*   The method `Entity<0>::count<codim>` is deprecated. Please use `Entity<0>::subEntities(uint codim)` instead.
*   The values `Geometry::dimension` and `Geometry::dimensionworld` are deprecated, and will be removed after the release of `dune-grid-2.4`.
*   The constructors of `YaspGrid`, that take a communicator as its first argument are deprecated. Please use the new constructors instead. The old ones will be removed after the release of `dune-grid-2.4`.
*   The capability class `Dune::Capabilities<GridType>::isParallel` is deprecated because its meaning was never well-defined. It will be removed after the 2.4 release. To suppress the deprecation warning define the macro `DUNE_AVOID_CAPABILITIES_IS_PARALLEL_DEPRECATION_WARNING`.
*   The `map` methods on mappers are deprecated and will be removed after the 2.4 release. They are replaced by the `index` and `subIndex` method (see above).
*   The method `geomTypes` on index sets is deprecated and will be removed after the 2.4 release. It is replaced by the new `types` method (see above).
*   The methods `lbegin`, `lend`, `leafbegin`, and `leafend` on grids are deprecated and will be removed after the 2.4 release. Use instead the methods `begin` and `end` from the grid's level and leaf grid view.
*   The `EntityPointer` is deprecated and will be removed after the 2.4 release. Instead, it is now possible to store and copy entities directly. All interface methods that used to return an `EntityPointer` now return an `Entity` instead. In order to ease the transition, entities can be used like an `EntityPointer`, but doing so will also raise deprecation warnings.
*   In line with the `EntityPointer` removal, the method `entityPointer(const EntitySeed&)` on grids has been deprecated. Please use the new method `entity(const EntitySeed&)` instead, which returns an `Entity`.
*   Comparisons between different types of entity iterators (level vs. leaf) and `EntityPointer` have been deprecated. Those kinds of comparisons should be replaced by comparisons between the entities pointed at by the iterators, so `leaf_it == level_it` becomes `*leaf_it == *level_it`. Iterators of a single type can of course still be compared with each other.
*   Support for Grape has been deprecated and will be removed after the 2.4 release. Use the Dune module [dune-grape](https://gitlab.dune-project.org/robert.kloefkorn/dune-grape) instead.

### Changes for maintainers of grid implementations

*   All grid implementations not using the `DefaultGridView` must rename their implementations from `leafView` / `levelView` to `leafGridView` / `levelGridView`.
*   Grid must be ported to the new copyable entities and intersections. See this [How-To](/doc/guides/copyable_entities_and_intersections) for some pointers.
*   Grids are now allowed to return temporary objects from their entity and intersection iterators. This is mostly interesting for meta grid developers, as it allows for a much more straightforward implementation.

## dune-istl

### Deprecated and removed features

*   The transition header `<dune/istl/diagonalmatrix.hh>` has been removed. The original header was moved to dune-common in Dune 2.2, use this instead.

## dune-localfunctions

*   All LocalFiniteElement classes now have a method `size`, which returns the number of shape functions of the finite element. This method is for convenience: previously, the `localBasis` had to be fetched to get the same information.
*   The `MonomLocalFiniteElement` class has been renamed to `MonomialLocalFiniteElement`. Correspondingly, its header `monom.hh` has been renamed to `monomial.hh`. The old class and header are still there for backward-compatibility.
*   New Raviart-Thomas elements were added but only for interpolation as the Jabobians are missing. The new elements are Raviart-Thomas 3 and 4 for 2d quadrilaterals. Thanks to Jizhou Li.
*   You can now get second derivatives of the shape functions of the `Pk2DLocalFiniteElement`. Thanks to Elisa Friebel.

### Deprecated and removed features

*   The class `Q2LocalFiniteElement`, deprecated in Dune 2.3 has been removed. Please use the more general `QkLocalFiniteElement` instead.
*   The class `RannacherTurek2DLocalFiniteElement<D, R>`, deprecated in Dune 2.3, has been removed. Please use `RannacherTurekLocalFiniteElement<D, R, 2>` instead.
*   Most of the Raviart-Thomas and Brezzi-Douglas-Marini elements not following the new naming scheme have been removed.

## dune-grid-howto

## dune-grid-dev-howto

*   The whole module was deprecated with DUNE 2.3 and has been abandoned. `IdentityGrid` has been moved to dune-grid.

# Known Bugs

*   The pseudo inverse used in the generic geometries might fail for nearly singular matrices. This is not really a problem unless the grid is nearly degenerate.
*   The parallel `UGGrid` may return wrong ids on very complex adaptively refined and load-balanced grids. The reason is that the Dune grid interface mandates that two entities on different levels have the same id if they are copies. Therefore the UGGrid id/subId methods look through the ancestry of a given entity to see if there are copies. However, UG does so-called vertical load-balancing, which means that the ancestry may be distributed across different processors. Since the current code does not take that into account, wrong ids will be returned in the presence of vertical load-balancing. This is a potentially severe issue, because users do not get error messages, only ids that are tacitly wrong.
*   Building shared libraries with CMake might break if you are using external static libraries compiled without support for position independent code (g++ -fpic).
*   Dune-grid fails to build with GCC 4.4 when configured with UGGrid. This is due to an overload resolution failure in GCC 4.4 that is no longer present in newer compilers. (See [this bug](https://gitlab.dune-project.org/flyspray/FS/issues/1695).)
*   You can only build dune with GCC 4.9 without the `-pedantic` flag because it will otherwise reject some compatibility code needed to support GCC 4.4. (See [this bug](https://gitlab.dune-project.org/flyspray/FS/issues/1634).)
*   Dune-istl does not work with METIS 5, see [this bug](https://gitlab.dune-project.org/flyspray/FS/issues/1212).
*   Dune does not build on debian-ish systems if **all** the following conditions are satisfied: The system uses GCC 4.9 or newer as default compiler, you are building with GCC 4.8 or below, and you are using MPICH. The reason is [this bug](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=791676) which makes mpi propose flags from the newer default compiler also for older compilers where they are not supported.

A list of all bugs can be found online in our bug-tracker [http://www.dune-project.org/flyspray/](http://www.dune-project.org/flyspray/).
