+++
# The name of the module.
module = "dune-python"

# Groups that this module belongs to, please specify (otherwise your module will not
# be reachable from the menu through the groups).
# Currently recognized groups: "core", "disc", "grid", "external", "extension", "user"
group = ["extension"]

# List of modules that this module requires
requires = [ "dune-common", "dune-geometry", "dune-grid", "dune-istl", "dune-localfunctions" ]

# A string with maintainers to be shown in short description, if present.
maintainers = "Andreas Dedner, Martin Nolte"

# Main Git repository, uncomment if present
git = "https://gitlab.dune-project.org/staging/dune-python.git"

# Short description (like one sentence or two). For a more detailed description,
# just write as much as you want in markdown below, uncomment if present.
short = "Python bindings for the DUNE core modules"
+++

This module provides python binding for all the dune core modules. This
makes it possible to use Python to perform pre and post processing steps
and to test new algorithms using Python before transferring the code
to C++. The modular structure of DUNE and its interface based approach
are maintained as far as possible. We use just in time compilation to
dynamically add interface realizations found in any installed DUNE
module and to compile additional C++ functions on demand.

We have attempted to export all DUNE C++ classes to Python with an
identical interface while also providing additional the functionality which
would be expected by Python users. The aim was to make the transition from
C++ to Python and vice versa as painless as possible.
