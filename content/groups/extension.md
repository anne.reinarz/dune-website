+++
group = "extension"
title = "Extension Modules"
[menu.main]
parent = "modules"
weight = 4
+++

Some Dune modules do extend the functionality of Dune greatly while not being grid modules.
These are listed in this category.
