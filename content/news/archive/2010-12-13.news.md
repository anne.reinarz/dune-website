+++
date = "2010-12-13"
title = "DUNE Spring Course, March 21-25 2011"
+++

IWR, University Heidelberg, Germany

<http://conan.iwr.uni-heidelberg.de/dune-workshop/>

This one week course will provide an introduction to the most important DUNE modules. At the end the attendees will have a solid knowledge of the simulation workflow from mesh generation and implementation of finite element and finite volume methods to visualization of the results. Successful participation requires knowledge of object-oriented programming using C++ including generic programming with templates (this knowledge will be brushed up on the first day of the course). A solid background on numerical methods for the solution of PDEs is expected.

Topics cover:

-   Review of C++ programming techniques
-   DUNE Grid interface
-   Grid IO (pre- and postprocessing)
-   DUNE PDELab
-   Mesh Adaptivity
-   Parallel computations
-   Iterative Solvers
-   Nonlinear problems
-   Time-dependent problems

For registration and further information see <http://conan.iwr.uni-heidelberg.de/dune-workshop/index.html>
