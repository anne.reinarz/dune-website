+++
date = "2010-10-25"
title = "UG-3.9.1-patch3 Patch File Released"
+++

A new patch file for the UG grid manager has been released. It contains a new flag for UG nodes, to check for leaf nodes. This patch is needed for upcoming changes in the dune UG interface. These changes will fix several problems when running ug in parallel.
