+++
date = "2014-05-29"
title = "DUNE is part of ESA Summer of Code in Space 2014"
+++

DUNE participates in this year's ESA Summer of Code in Space ([SOCIS 2014](http://sophia.estec.esa.int/socis2014/)). Within the next three months, Marco Agnese from the Imperial College of London will add [thread support to parallel index sets](http://users.dune-project.org/projects/esa-socis-2014/wiki/Project_ideas#Project-1-Add-thread-support-to-parallel-index-sets).

Marco writes in a [blog](http://marcoagnese.blogspot.co.uk/) about his ongoing work.
