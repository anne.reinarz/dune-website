+++
date = "2011-04-09"
title = "Warwick DUNE Summer School, June 20-24 2011"
+++

University of Warwick, UK

This one week course will give an introduction to the DUNE core modules including the DUNE grid interface library, and the [DUNE-FEM](http://dune.mathematik.uni-freiburg.de/) module.

The course will focus on the numerical treatment of evolution equations of the form

∂<sub>t</sub>U + ∇ · ( F(U) - D(U) ∇U ) + S(U) = 0

using finite-volume, continuous, and discontinuous Galerkin methods. These schemes will be implemented with the DUNE-FEM module.

Further information and a registration from can be found [here](http://www.warwick.ac.uk/go/dune).
