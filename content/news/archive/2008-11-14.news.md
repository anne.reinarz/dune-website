+++
date = "2008-11-14"
title = "New UG Patch File"
+++

A new patch file for UG has been uploaded and can be found [here](/doc/install-ug/). It includes a new method that is needed to properly implement the `boundaryId()` method. If you use `UGGrid` and the DUNE developer branch you have to get this new patch file before updating `dune-grid`. Sorry for the hassle.
