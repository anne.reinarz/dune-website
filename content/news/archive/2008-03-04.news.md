+++
date = "2008-03-04"
title = "New ALUGrid Version"
+++

ALUGrid version 1.1 has been released. This will be the stable version for the DUNE trunk as well as the upcoming release 1.0.1 of the DUNE core modules. ALUGrid has been revised to work with a large number of grid cells (more than 100 million) in parallel. Also some minor bug fixes have been done. The new version is available from the [ALUGrid page](http://www.mathematik.uni-freiburg.de/IAM/Research/alugrid/).
