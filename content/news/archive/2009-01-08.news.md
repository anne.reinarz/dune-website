+++
date = "2009-01-08"
title = "DUNE Course March 25-27, 2009"
+++

IWR, University Heidelberg, Germany

http://conan.iwr.uni-heidelberg.de/dune-workshop/

In many areas of science and engineering numerical simulation is an important tool for research and development. Current simulation trends range from multiscale-/multiphysics modelling to the usage of parallel machines with PetaFlops performance. The Distributed and Unified Numerics Environment (DUNE) tries to fit these heterogeneous requirements inside a single environment with novel numerical techniques as well as state-of-the-art software development methods.

By participating in this course scientists have the opportunity to get a hands-on introduction to the DUNE framework. Main focus is to give a detailed introduction to the DUNE core modules: the grid interface including IO methods with its numerous grid implementations and the iterative solver module ISTL. In the exercises elliptic and hyperbolic model problems will be solved with various methods.

In addition, a course on advanced C++ programming will be held before the DUNE workshop on March 23-24, 2009.
