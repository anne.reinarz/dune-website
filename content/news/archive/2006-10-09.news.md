+++
date = "2006-10-09"
title = "New ALUGrid version"
+++

Dune got updated to the latest version of ALUGrid.

The current version of Dune-ALUGrid will need the latest ALUGrid-library with version number 0.4 to compile. See the librarys ChangeLog for new features and a bug-fix list.
