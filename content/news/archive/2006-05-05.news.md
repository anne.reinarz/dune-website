+++
date = "2006-05-05"
title = "Great things are going on."
+++

After a longer discussion on the Dune mailing list we decided to split the Dune repository into several repositories according to the module structure. Together with this change we decided to switch our revision control system from CVS to subversion.

After a lot lot hard work the transition is finished. All Dune modules have their own subversion repository. The nightly builds are running again and the webpage is created from the new sources.

Still there are some open issues, especially the documentation is out of sync with the latest changes. For a list of open issues see [Peters mail](http://dune-project.org/pipermail/dune/2006-April/001369.html).
