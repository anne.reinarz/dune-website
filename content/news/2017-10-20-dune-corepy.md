+++
date = "2017-10-20"
title = "dune-python f.k.a. dune-corepy"
+++

It was decided at the 2016 dune developer meeting that `dune-corepy` should
be renamed into `dune-python`. This has now been done.
A [transition guide] is available.

  [transition guide]: https://gitlab.dune-project.org/staging/dune-python/blob/master/UPGRADE.md

